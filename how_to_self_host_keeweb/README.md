# How to self-host KeeWeb

## Introduction to KeeWeb

KeeWeb ([official website](https://keeweb.info/)) is a web-based implementation of KeePass ([official website](https://keepass.info/)), an open source and offline password manager.

KeeWeb is available via web browsers on all platforms including mobile, and also featured as an Electron ([official website](https://www.electronjs.org/)) app on Windows, Linux and Mac operating systems.

## Introduction to self-hosting

Self-hosting consists in hosting a service on the user's own server.

Well-known examples of self-hostable services are WordPress for blog editing and Nextcloud for file sharing.

However, the case of KeeWeb is specific because it has no server-side : it only consists in a progressive web app ([Wikipedia](https://en.wikipedia.org/wiki/Progressive_web_application)) with front-end features.

Therefore, the only purpose of self-hosting KeeWeb is self-hosting your password database file (`.kdbx`), thus saving you time not having to import it from your device/cloud.

## Introduction to WebDAV

WebDAV [(Wikipedia](https://en.wikipedia.org/wiki/WebDAV)) is a file transfer feature implemented within the HTTP protocol, allowing web applications like KeeWeb to upload a file from the client to the server without requiring any back-end.

## Requirements

For this tutorial, we'll be using an Apache ([official website](https://httpd.apache.org/)) web server on Debian ([official website](https://www.debian.org/)) operating system, assuming that you know the basics of them.

KeeWeb also requires a valid HTTPS certificate, therefore, you also need a domain name and eventually a subdomain, assuming that you know the basics of managing domains.

## Step 1/3 — Download & extract KeeWeb

KeeWeb's release files are available at the project's repository ([GitHub](https://github.com/keeweb/keeweb/releases)), we're more specifically looking for the latest `.html.zip` file, which only contains the web files.

### Method 1 : server-side

**NOTE :** pay attention to version number if you copy/paste the commands.

- Download the archive into the server
  <br>`wget https://github.com/keeweb/keeweb/releases/download/v1.14.0/KeeWeb-1.14.0.html.zip`

- Extract the archive
  <br>`unzip KeeWeb-1.14.0.html.zip`
  <br>(**NOTE** : files will be extracted in the current folder)

### Method 2 : client-side

- Download the archive into your computer
- Extract the archive
- Upload the extracted files to your server

## Step 2/3 — Import your KDBX file(s)

### Upload

In your KeeWeb installation directory :

- Create a folder which will contain your KDBX file(s)
  `mkdir kdbx`
- Upload your KDBX file(s) into it

### Configure

In your KeeWeb installation directory :

- Edit the `index.html` file
- Find `<meta name="kw-config" content="(no-config)">`
- Replace by `<meta name="kw-config" content="./config.json">`
- Create a `config.json` file

JSON, aka. JavaScript Object Notation ([Wikipedia](https://en.wikipedia.org/wiki/JSON)) is standardized data format, which, in this case, allows us to define KeeWeb's configuration.

The KeeWeb configuration is divided in two parts called `settings` (allowing to define the app's default behavior) and `files` (allowing to specify the default KDBX files to import).

Here's an example :

```json
{
    "settings": {
        "theme": "te",
        "locale": "en",
        "canOpen": false,
        "canOpenDemo": false,
        "canOpenSettings": false,
        "canCreate": false,
        "canImportXml": false,
        "dropbox": false,
        "webdav": false,
        "gdrive": false,
        "onedrive": false
    },
    "files": [
        {
            "storage": "webdav",
            "name": "Example file #1",
            "path": "./kdbx/example.kdbx"
        },
        {
            "storage": "webdav",
            "name": "Example file #2",
            "path": "./kdbx/example2.kdbx"
        }
    ]
}
```

The `theme` key accepts the following values to specify a theme :

- `fb` for *Flat blue*
- `db` for *Dark brown*
- `sd` for *Solarized dark*
- `sl` for *Solarized light*
- `wh` for *White*
- `te` for *Terminal*
- `hc` for *High contrast*

The `locale` key accepts the following values to specify a theme :

- `en` for *English*
- `de-DE` for *German*
- `fr-Fr` for *French*

The rest defines whether the user can open files from external sources. In our use case, these features are useless, so they're all disabled.

The `files` key is a list of files that can be opened out of the box, that's what we're interested in.

A file item has :

- a `storage` key, which value will always be `webdav` for our use case
- a `name` key, which value will be displayed in your app's home page
- a `path` key, which value points to the KDBX file in your server

## Step 3/3 — Setup Apache

### Create a virtual host configuration

As you should know, Apache virtual hosts configuration files are located at `/etc/apache2/sites-available`.

If you want to create a domain or subdomain for your KeeWeb installation :

- Create a `.conf` file

If you want to use an existing domain or subdomain for your KeeWeb installation :

- Edit your existing configuration file

### Setup a domain or subdomain

If you already have a ready-to-use domain or subdomain with HTTP and HTTPS working, you can skip this step.

- Create a virtual host
    For HTTP :
  
  ```
  <VirtualHost *:80>
      ServerName kw.example.com
      RewriteEngine On
      RewriteCond %{HTTPS} off
      RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
  </VirtualHost>
  ```
  
    For HTTPS :
  
  ```
  <VirtualHost *:443>
      ServerName kw.example.com
      DocumentRoot /your/keeweb/path
      SSLCertificateFile /your/public/key/path
      SSLCertificateKeyFile /your/private/key/path
  </VirtualHost>
  ```

- Replace `ServerName` (for HTTP and HTTPS) with your domain/subdomain
  <br>([documentation](https://httpd.apache.org/docs/2.4/en/mod/core.html#servername))

- Replace `DocumentRoot` (for HTTP and HTTPS) with your KeeWeb install path
  <br>([documentation](https://httpd.apache.org/docs/2.4/en/mod/core.html#documentroot))

- Replace `SSLCertificateFile` with your SSL public key path
  <br>([documentation](https://httpd.apache.org/docs/2.4/fr/mod/mod_ssl.html#sslcertificatefile))

- Replace `SSLCertificateKeyFile` with your SSL private key path
  <br>([documentation](https://httpd.apache.org/docs/2.4/fr/mod/mod_ssl.html#sslcertificatekeyfile))

If you're setting up a new domain or subdomain, you must create, in your domain provider's DNS zone, a new entry, of type `A`, with your server's IP address as value.

### Setup WebDAV

WebDAV is disabled my default. Therefore, you need to enable it in your configuration file for the directory containing your KDBX file(s) by specifying its absolute path.

- Specify the path according to your file system (recommended)
  
  ```
  <Directory "/var/www/kw.example.com/kdbx">
      DAV On
      Options Indexes
  </Directory>
  ```
  
  ([documentation](https://httpd.apache.org/docs/2.4/en/mod/core.html#directory))

or

- Specify the path according to your web structure
  
  ```
  <Location "/kdbx">
      DAV On
      Options Indexes
  </Location>
  ```
  
  ([documentation](https://httpd.apache.org/docs/2.4/en/mod/core.html#location))

### Enable modules

SSL and WebDAV are modules and must be enabled prior to applying the configuration, using the `a2enmod` *("Apache 2 enable module")* command.

If you configured SSL for the first time :

- `a2enmod ssl`

If you configured WebDAV for the first time :

- `a2enmod dav`
- `a2enmod dav_fs`

([documentation](https://man.cx/a2enmod))

### Enable virtual host

As you should know, a newly created virtual host configuration must be enabled using the `a2ensite` (*"Apache 2 enable website"*) command, and specifiying your configuration file's name as only parameter.

- `a2ensite keeweb.conf `

This command will create a symbolic link ([Wikipedia](https://en.wikipedia.org/wiki/Symbolic_link)) from `/etc/apache2/sites-available` to `/etc/apache2/sites-enabled`.

([documentation](https://man.cx/a2ensite))

### Restart Apache

As you should know, the Apache service must be restarted after changing its configuration, in order to apply it.

- `systemctl restart apache2`

([documentation](https://man.cx/systemctl))

## Troubleshooting

### Apache fails to restart

You'll find detailed Apache errors :

- using the `systemctl status apache2` command
- using the `journalctl -xe ` command ([documentation](https://man.cx/journalctl))
- in the `/var/log/apache2/error.log` file

### KeeWeb can't open

Make sure that file paths are correct :

- in your Apache `.conf` file located at `/etc/apache2/sites-available`
- in your KeeWeb `config.json` file located at your installation path

Make sure that Apache's user has read permissions on the KDBX folder, see below.

### KeeWeb can't sync

Make sure that Apache's user has read and write permissions on the KDBX folder.

The recommanded way : your user is owner of all files by default, it can stay that way, but Apache's user can be set as group user, to be given appropriate permissions.

From your KeeWeb installation directory :

- Set Apache's user as group
  <br>`chgrp -R www-data kdbx`
  <br>([documentation](https://man.cx/chgrp))
- Allow group user to read and write
  <br>`chmod -R g+rw kdbx`
  <br>([documentation](https://man.cx/chmod))

**NOTE :** I experienced issues specifically on Firefox browser which may not be resolvable for now. Please try Chromium ([maintainer website](https://chromium.woolyss.com/download/)) or Chromium-based browsers.

### Reset KeeWeb

After changing KeeWeb's configuration, you may need to clear the app's data (local storage) from your web browser.